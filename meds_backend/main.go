package main

import (
	"bufio"
	"context"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strings"

	"meds"
	mod "models"

	"google.golang.org/grpc"
	glog "google.golang.org/grpc/grpclog"
)

// strings.Replace(inputStr, " ", "", -1)
// 	strings.Replace(inputStr, "\t", "", -1)
// 	strings.Replace(inputStr, "\n", "", -1)

var grpcLog glog.LoggerV2

func init() {
	grpcLog = glog.NewLoggerV2(os.Stdout, os.Stdout, os.Stdout)
}
func getMedNamesDropdown(inputText string) []string {
	var previousMedName string
	var medications []string
	getFile, err := os.Open("./data/medications.csv")
	if err != nil {
		grpcLog.Errorf("Error opening medications csv: %v", err)
	}
	defer getFile.Close()
	file := csv.NewReader(bufio.NewReader(getFile))
	fmt.Println(previousMedName)
	for {
		line, err := file.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		inputText = strings.ToUpper(inputText)
		if strings.Contains(line[6], inputText) && line[6] != previousMedName && !strings.Contains(line[6], "**") {

			// var strength = strings.Replace(line[3], " ", "", -1)
			// var name = strings.Replace(line[4], " ", "", -1)

			previousMedName = line[6]
			medications = append(medications, line[6])
		}

	}
	fmt.Printf("Medications list: %v", medications)
	return medications
}

func getMedicationDetails(inputText string) mod.Medication {
	var medication mod.Medication
	getFile, err := os.Open("./data/medications.csv")
	if err != nil {
		grpcLog.Errorf("Error opening medications csv: %v", err)
	}

	file := csv.NewReader(bufio.NewReader(getFile))
	defer getFile.Close()
	for {
		line, err := file.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		if line[6] == inputText {
			fmt.Printf("Input Text value and type: %v %T\n", inputText, inputText)
			fmt.Printf("Line Text value and type: %v %T\n", line[6], line[6])
			if len(medication.Strength) == 0 {
				medication.Strength = append(medication.Strength, line[3])
			}
			for i, v := range medication.Strength {
				if line[3] == v {
					break
				}

				if len(medication.Strength)-1 == i {
					medication.Strength = append(medication.Strength, line[3])
				}
			}

			if len(medication.DrugName) == 0 {
				medication.DrugName = line[6]
			}
		}
	}
	fmt.Println(medication.DrugName)
	fmt.Println(medication.Strength[0])
	return medication
}

type Server struct {
	meds.UnimplementedMedicationServer
}

func (*Server) GetSelectedMedDetails(ctx context.Context, req *meds.SelectedMedDetailsRequest) (*meds.SelectedMedDetailsResponse, error) {
	fmt.Printf("Get med details function called: %v\n", req)
	inputStr := req.SelectedMedName
	result := getMedicationDetails(inputStr)
	res := &meds.SelectedMedDetailsResponse{
		MedDetails: &meds.MedDetails{MedName: result.DrugName, Strength: result.Strength},
	}

	return res, nil
}

func (*Server) PopulateMedDropdown(ctx context.Context, req *meds.MedNameRequest) (*meds.MedNameResponse, error) {
	fmt.Printf("Populate dropdown function called: %v\n", req)
	inputStr := req.MNReq
	fmt.Printf("check string: %v\n", inputStr)

	result := getMedNamesDropdown(inputStr)
	res := &meds.MedNameResponse{
		MNRes: result,
	}

	return res, nil
}

// layout := "2006-01-02T15:04:05.000Z"
// emailTimer := time.Now().String()
// currentTime := time.Now().Format(layout)
// ct, _ := time.Parse(time.RFC3339, currentTime)
// fmt.Println("current time:", ct)
// et, _ := time.Parse(time.RFC3339, emailTimer)
// fmt.Println("Email timer before adding 30 min: ", et)

func main() {

	lis, err := net.Listen("tcp", ":3000") //defaults to localhost:3000
	if err != nil {
		log.Fatalf("Server Failed to listen: %v", err)
	}
	fmt.Println("Server Running on localhost:3000")

	grpcserver := grpc.NewServer()
	meds.RegisterMedicationServer(grpcserver, &Server{})

	if err := grpcserver.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}

}
