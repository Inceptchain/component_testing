import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:grpc/grpc.dart';
import 'package:protos/protos.dart';

class MedsNotifier extends StateNotifier<List<String>> {
  MedsNotifier() : super(const []);

  //problems with reaching the GRPC server may occur, if they do use initState
  MedicationClient client = MedicationClient(
      ClientChannel(
        //set this to the api address once its up or gateway
        "10.0.2.2", //"10.0.2.2"
        port: 3000,
        options: const ChannelOptions (
          credentials: ChannelCredentials.insecure(),
        ),
      ),
    );

  // medications() {
    // client = MedicationClient(
    //   ClientChannel(
    //     //set this to the api address once its up or gateway
    //     "192.168.1.71", //"192.168.0.24"
    //     port: 3000,
    //     options: const ChannelOptions (
    //       credentials: ChannelCredentials.insecure(),
    //     ),
    //   ),
    // );
  // }


  // Future<void> loadMeds() async {
  // }

  // void addMed(Medication med) async {
    
  // }

  Future<void> getNamesDropdown(String data) async {
    final response = await client.populateMedDropdown(MedNameRequest()..mNReq = data);
    state = response.mNRes;
  }

}

final medsProvider = StateNotifierProvider<MedsNotifier, List<String>>(
  (ref) => MedsNotifier(),
);
