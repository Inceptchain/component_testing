module medications

replace meds => ../protos/go_meds/meds

replace models => ./models

require (
	github.com/golang/protobuf v1.5.3
	meds v1.0.0
	golang.org/x/net v0.14.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20230803162519-f966b187b2e5 // indirect
	google.golang.org/grpc v1.57.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.3.0 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
	models v1.0.0
)