//
//  Generated code. Do not modify.
//  source: meds.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use medDetailsDescriptor instead')
const MedDetails$json = {
  '1': 'MedDetails',
  '2': [
    {'1': 'medName', '3': 1, '4': 1, '5': 9, '10': 'medName'},
    {'1': 'strength', '3': 2, '4': 3, '5': 9, '10': 'strength'},
  ],
};

/// Descriptor for `MedDetails`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List medDetailsDescriptor = $convert.base64Decode(
    'CgpNZWREZXRhaWxzEhgKB21lZE5hbWUYASABKAlSB21lZE5hbWUSGgoIc3RyZW5ndGgYAiADKA'
    'lSCHN0cmVuZ3Ro');

@$core.Deprecated('Use selectedMedDetailsRequestDescriptor instead')
const SelectedMedDetailsRequest$json = {
  '1': 'SelectedMedDetailsRequest',
  '2': [
    {'1': 'selectedMedName', '3': 1, '4': 1, '5': 9, '10': 'selectedMedName'},
  ],
};

/// Descriptor for `SelectedMedDetailsRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List selectedMedDetailsRequestDescriptor = $convert.base64Decode(
    'ChlTZWxlY3RlZE1lZERldGFpbHNSZXF1ZXN0EigKD3NlbGVjdGVkTWVkTmFtZRgBIAEoCVIPc2'
    'VsZWN0ZWRNZWROYW1l');

@$core.Deprecated('Use selectedMedDetailsResponseDescriptor instead')
const SelectedMedDetailsResponse$json = {
  '1': 'SelectedMedDetailsResponse',
  '2': [
    {'1': 'medDetails', '3': 1, '4': 1, '5': 11, '6': '.meds.MedDetails', '10': 'medDetails'},
  ],
};

/// Descriptor for `SelectedMedDetailsResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List selectedMedDetailsResponseDescriptor = $convert.base64Decode(
    'ChpTZWxlY3RlZE1lZERldGFpbHNSZXNwb25zZRIwCgptZWREZXRhaWxzGAEgASgLMhAubWVkcy'
    '5NZWREZXRhaWxzUgptZWREZXRhaWxz');

@$core.Deprecated('Use medNameRequestDescriptor instead')
const MedNameRequest$json = {
  '1': 'MedNameRequest',
  '2': [
    {'1': 'mNReq', '3': 1, '4': 1, '5': 9, '10': 'mNReq'},
  ],
};

/// Descriptor for `MedNameRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List medNameRequestDescriptor = $convert.base64Decode(
    'Cg5NZWROYW1lUmVxdWVzdBIUCgVtTlJlcRgBIAEoCVIFbU5SZXE=');

@$core.Deprecated('Use medNameResponseDescriptor instead')
const MedNameResponse$json = {
  '1': 'MedNameResponse',
  '2': [
    {'1': 'mNRes', '3': 1, '4': 3, '5': 9, '10': 'mNRes'},
  ],
};

/// Descriptor for `MedNameResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List medNameResponseDescriptor = $convert.base64Decode(
    'Cg9NZWROYW1lUmVzcG9uc2USFAoFbU5SZXMYASADKAlSBW1OUmVz');

