import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:meds_flutter/providers/medication_provider.dart';

class MedicationsScreen extends ConsumerStatefulWidget {
  const MedicationsScreen({super.key});

  @override
  ConsumerState<MedicationsScreen> createState() {
    return _MedicationsScreen();
  }
}

class _MedicationsScreen extends ConsumerState<MedicationsScreen> {
  //late Future<void> _medsFuture;

  // @override
  // void initState() {
  //   super.initState();
  //   _medsFuture = ref.read(medsProvider.notifier).gMeds();
  // }

  @override
  Widget build(BuildContext context) {
    final _meds = ref.read(medsProvider.notifier);
    final _medList = ref.watch(medsProvider);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Select Medication'),
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 3, right: 3),
              child: TextField(
                decoration: const InputDecoration(
                  contentPadding: EdgeInsets.only(left: 10),
                ),
                onChanged: (value) async {
                  await _meds.getNamesDropdown(value);
                },
              ),
            ),
            _medList.isNotEmpty
                ? Expanded(
                    child: ListView.builder(
                      itemCount: _medList.length,
                      itemBuilder: (ctx, i) {
                        return ListTile(
                          title: Text(
                            _medList[i],
                            style: const TextStyle(color: Colors.black),
                          ),
                        );
                      },
                    ),
                  )
                : const Column(
                    children: [
                      SizedBox(
                        height: 150,
                      ),
                      Text('Search for a new medication'),
                    ],
                  ),
          ],
        ),
      ),
    );
  }
}
