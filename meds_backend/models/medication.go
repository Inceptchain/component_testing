package models

type Medication struct {
	Strength []string
	DrugName string
}
