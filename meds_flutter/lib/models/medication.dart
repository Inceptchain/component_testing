class Medication {
  Medication({
    required this.drugName,
    this.quantity,
    this.type,
    this.route,
    this.frequency,
    this.strength,
    this.timeStamp,
    this.reason,
    this.active,
  }); 
  
  final String drugName;
  final String? quantity;
  final String? type;
  final String? route;
  final String? frequency;
  final String? strength;
  final String? timeStamp;
  final String? reason;
  final bool? active;
}

List<String> frequency = ['once daily', 'at bedtime', 'two times a day', 'three times a day', 'four times a day', 'with meals', 'with meals and at bedtime', 'every 1 hour', 'every 2 hours', 'every 4 hours', 'every 6 hours', 'every 8 hours', 'every 12 hours', 'every other day', 'weekly', 'monthly', 'as needed', 'OTHER'];
List<String> drugRoute = ['by injection', 'by mouth', 'in the ear', 'in the eye', 'in the nose', 'inhaled', 'rectal', 'through feeding tube', 'through an intravenous(IV) line', 'through the skin', 'under the tongue', 'vaginal'];
List<String> quantityList = ['1/4', '1/2', '1', '1.5', '2', '2.5', '3', '3.5', '4', '4.5', '5', '5.5', '6', '6.5', '7', '7.5', '8', '8.5', '9', '9.5', '10', '10.5', '11', '11.5', '12', '12.5', '13', '13.5', '14', '14.5', '15', '15.5', '16', '16.5', '17', '17.5', '18', '18.5', '19', '19.5', '20', '20.5', '21', '21.5', '22', '22.5', '23', '23.5', '24', '24.5', '25', '25.5', '26', '26.5', '27', '27.5', '28', '28.5', '29', '29.5', '30', '30.5', '31', '31.5', '32', '32.5', '33', '33.5', '34', '34.5', '35', '35.5', '40' ];
List<String> type = ['Tablet', 'Capsule', 'Dropper', 'Syringe', 'Units'];
List<String> taking = ['Yes', 'No'];

