# medications service

### **Create and Run Docker container for medications server**
* Make sure the network is up and running first
* Make sure the ./network.sh file has also been run
* From medications folder run this command:  
> docker build -t medications .  
* Run this command once docker is done building the image:  
> docker run -p 3001:3001 --network=? -it medications bash
* Run this command in the shell that is created:  
> go run main.go


