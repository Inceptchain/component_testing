//
//  Generated code. Do not modify.
//  source: meds.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:async' as $async;
import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'package:protobuf/protobuf.dart' as $pb;

import 'meds.pb.dart' as $0;

export 'meds.pb.dart';

@$pb.GrpcServiceName('meds.Medication')
class MedicationClient extends $grpc.Client {
  static final _$populateMedDropdown = $grpc.ClientMethod<$0.MedNameRequest, $0.MedNameResponse>(
      '/meds.Medication/PopulateMedDropdown',
      ($0.MedNameRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.MedNameResponse.fromBuffer(value));
  static final _$getSelectedMedDetails = $grpc.ClientMethod<$0.SelectedMedDetailsRequest, $0.SelectedMedDetailsResponse>(
      '/meds.Medication/GetSelectedMedDetails',
      ($0.SelectedMedDetailsRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.SelectedMedDetailsResponse.fromBuffer(value));

  MedicationClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options,
        interceptors: interceptors);

  $grpc.ResponseFuture<$0.MedNameResponse> populateMedDropdown($0.MedNameRequest request, {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$populateMedDropdown, request, options: options);
  }

  $grpc.ResponseFuture<$0.SelectedMedDetailsResponse> getSelectedMedDetails($0.SelectedMedDetailsRequest request, {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getSelectedMedDetails, request, options: options);
  }
}

@$pb.GrpcServiceName('meds.Medication')
abstract class MedicationServiceBase extends $grpc.Service {
  $core.String get $name => 'meds.Medication';

  MedicationServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.MedNameRequest, $0.MedNameResponse>(
        'PopulateMedDropdown',
        populateMedDropdown_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.MedNameRequest.fromBuffer(value),
        ($0.MedNameResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SelectedMedDetailsRequest, $0.SelectedMedDetailsResponse>(
        'GetSelectedMedDetails',
        getSelectedMedDetails_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SelectedMedDetailsRequest.fromBuffer(value),
        ($0.SelectedMedDetailsResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.MedNameResponse> populateMedDropdown_Pre($grpc.ServiceCall call, $async.Future<$0.MedNameRequest> request) async {
    return populateMedDropdown(call, await request);
  }

  $async.Future<$0.SelectedMedDetailsResponse> getSelectedMedDetails_Pre($grpc.ServiceCall call, $async.Future<$0.SelectedMedDetailsRequest> request) async {
    return getSelectedMedDetails(call, await request);
  }

  $async.Future<$0.MedNameResponse> populateMedDropdown($grpc.ServiceCall call, $0.MedNameRequest request);
  $async.Future<$0.SelectedMedDetailsResponse> getSelectedMedDetails($grpc.ServiceCall call, $0.SelectedMedDetailsRequest request);
}
