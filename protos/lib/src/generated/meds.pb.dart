//
//  Generated code. Do not modify.
//  source: meds.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class MedDetails extends $pb.GeneratedMessage {
  factory MedDetails() => create();
  MedDetails._() : super();
  factory MedDetails.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MedDetails.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'MedDetails', package: const $pb.PackageName(_omitMessageNames ? '' : 'meds'), createEmptyInstance: create)
    ..aOS(1, _omitFieldNames ? '' : 'medName', protoName: 'medName')
    ..pPS(2, _omitFieldNames ? '' : 'strength')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MedDetails clone() => MedDetails()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MedDetails copyWith(void Function(MedDetails) updates) => super.copyWith((message) => updates(message as MedDetails)) as MedDetails;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static MedDetails create() => MedDetails._();
  MedDetails createEmptyInstance() => create();
  static $pb.PbList<MedDetails> createRepeated() => $pb.PbList<MedDetails>();
  @$core.pragma('dart2js:noInline')
  static MedDetails getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MedDetails>(create);
  static MedDetails? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get medName => $_getSZ(0);
  @$pb.TagNumber(1)
  set medName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMedName() => $_has(0);
  @$pb.TagNumber(1)
  void clearMedName() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.String> get strength => $_getList(1);
}

class SelectedMedDetailsRequest extends $pb.GeneratedMessage {
  factory SelectedMedDetailsRequest() => create();
  SelectedMedDetailsRequest._() : super();
  factory SelectedMedDetailsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SelectedMedDetailsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'SelectedMedDetailsRequest', package: const $pb.PackageName(_omitMessageNames ? '' : 'meds'), createEmptyInstance: create)
    ..aOS(1, _omitFieldNames ? '' : 'selectedMedName', protoName: 'selectedMedName')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SelectedMedDetailsRequest clone() => SelectedMedDetailsRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SelectedMedDetailsRequest copyWith(void Function(SelectedMedDetailsRequest) updates) => super.copyWith((message) => updates(message as SelectedMedDetailsRequest)) as SelectedMedDetailsRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static SelectedMedDetailsRequest create() => SelectedMedDetailsRequest._();
  SelectedMedDetailsRequest createEmptyInstance() => create();
  static $pb.PbList<SelectedMedDetailsRequest> createRepeated() => $pb.PbList<SelectedMedDetailsRequest>();
  @$core.pragma('dart2js:noInline')
  static SelectedMedDetailsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SelectedMedDetailsRequest>(create);
  static SelectedMedDetailsRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get selectedMedName => $_getSZ(0);
  @$pb.TagNumber(1)
  set selectedMedName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSelectedMedName() => $_has(0);
  @$pb.TagNumber(1)
  void clearSelectedMedName() => clearField(1);
}

class SelectedMedDetailsResponse extends $pb.GeneratedMessage {
  factory SelectedMedDetailsResponse() => create();
  SelectedMedDetailsResponse._() : super();
  factory SelectedMedDetailsResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SelectedMedDetailsResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'SelectedMedDetailsResponse', package: const $pb.PackageName(_omitMessageNames ? '' : 'meds'), createEmptyInstance: create)
    ..aOM<MedDetails>(1, _omitFieldNames ? '' : 'medDetails', protoName: 'medDetails', subBuilder: MedDetails.create)
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SelectedMedDetailsResponse clone() => SelectedMedDetailsResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SelectedMedDetailsResponse copyWith(void Function(SelectedMedDetailsResponse) updates) => super.copyWith((message) => updates(message as SelectedMedDetailsResponse)) as SelectedMedDetailsResponse;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static SelectedMedDetailsResponse create() => SelectedMedDetailsResponse._();
  SelectedMedDetailsResponse createEmptyInstance() => create();
  static $pb.PbList<SelectedMedDetailsResponse> createRepeated() => $pb.PbList<SelectedMedDetailsResponse>();
  @$core.pragma('dart2js:noInline')
  static SelectedMedDetailsResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SelectedMedDetailsResponse>(create);
  static SelectedMedDetailsResponse? _defaultInstance;

  @$pb.TagNumber(1)
  MedDetails get medDetails => $_getN(0);
  @$pb.TagNumber(1)
  set medDetails(MedDetails v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasMedDetails() => $_has(0);
  @$pb.TagNumber(1)
  void clearMedDetails() => clearField(1);
  @$pb.TagNumber(1)
  MedDetails ensureMedDetails() => $_ensure(0);
}

class MedNameRequest extends $pb.GeneratedMessage {
  factory MedNameRequest() => create();
  MedNameRequest._() : super();
  factory MedNameRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MedNameRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'MedNameRequest', package: const $pb.PackageName(_omitMessageNames ? '' : 'meds'), createEmptyInstance: create)
    ..aOS(1, _omitFieldNames ? '' : 'mNReq', protoName: 'mNReq')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MedNameRequest clone() => MedNameRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MedNameRequest copyWith(void Function(MedNameRequest) updates) => super.copyWith((message) => updates(message as MedNameRequest)) as MedNameRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static MedNameRequest create() => MedNameRequest._();
  MedNameRequest createEmptyInstance() => create();
  static $pb.PbList<MedNameRequest> createRepeated() => $pb.PbList<MedNameRequest>();
  @$core.pragma('dart2js:noInline')
  static MedNameRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MedNameRequest>(create);
  static MedNameRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get mNReq => $_getSZ(0);
  @$pb.TagNumber(1)
  set mNReq($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMNReq() => $_has(0);
  @$pb.TagNumber(1)
  void clearMNReq() => clearField(1);
}

class MedNameResponse extends $pb.GeneratedMessage {
  factory MedNameResponse() => create();
  MedNameResponse._() : super();
  factory MedNameResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MedNameResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'MedNameResponse', package: const $pb.PackageName(_omitMessageNames ? '' : 'meds'), createEmptyInstance: create)
    ..pPS(1, _omitFieldNames ? '' : 'mNRes', protoName: 'mNRes')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MedNameResponse clone() => MedNameResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MedNameResponse copyWith(void Function(MedNameResponse) updates) => super.copyWith((message) => updates(message as MedNameResponse)) as MedNameResponse;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static MedNameResponse create() => MedNameResponse._();
  MedNameResponse createEmptyInstance() => create();
  static $pb.PbList<MedNameResponse> createRepeated() => $pb.PbList<MedNameResponse>();
  @$core.pragma('dart2js:noInline')
  static MedNameResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MedNameResponse>(create);
  static MedNameResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.String> get mNRes => $_getList(0);
}


const _omitFieldNames = $core.bool.fromEnvironment('protobuf.omit_field_names');
const _omitMessageNames = $core.bool.fromEnvironment('protobuf.omit_message_names');
