package models

//UserProfile struct
type UserProfile struct {
	SessID        string
	UserID        string
	Fname         string
	Lname         string
	Email         string
	Password      string
	UserType      string
	AgreeToTOS    bool
	LoggedIn      bool
	Phone         string
	StreetAddress string
	City          string
	State         string
	Zip           string
	NpiNum        string
	Gender        string
	Pronoun       string
	Dob           string
	Verified      bool
}
